# Task Manager

## Description
The Task Manager is a simple application for managing your tasks and to-do lists.

## Features
- Add tasks
- Delete tasks
- View list of tasks

## Installation
1. Clone the repository
2. Install the required dependencies using `pip install -r requirements.txt`
3. Run the application using `python run.py`

## Usage
- Once the application is running, you can access it through your web browser at `http://localhost:5000`
- As of now it doesn't have a UI, so need to use postman: `/`or  `/tasks`. POST, DELETE works  

