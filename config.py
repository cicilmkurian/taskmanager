import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# instance of Flask class
app = Flask(__name__)

project_dir = os.path.dirname(os.path.abspath(__file__))
db_file = "sqlite:///{}".format(os.path.join(project_dir, "database.db"))

app.config['SQLALCHEMY_DATABASE_URI'] = db_file
db = SQLAlchemy(app)
