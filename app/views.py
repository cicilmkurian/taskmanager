from config import  db,app
from app.models import Task
from flask import request,jsonify



@app.route('/',methods=['GET'])
def index():
    return "Welcome to task manager",200

@app.route('/tasks', methods=['GET'])
def get():
    tasks = Task.query.all()
    return {'Tasks':[task.to_dict() for task in tasks]}

@app.route('/tasks', methods=['POST'])
def create():
    task_data = request.get_json()
    task = Task(title=task_data['title'], description=task_data.get('description'), status=task_data.get('status','todo'),
                priority=task_data.get('priority','medium'))
    db.session.add(task)
    db.session.commit()
    return {'task': task.to_dict()}, 201

@app.route('/tasks/<int:task_id>', methods=['DELETE'])
def delete(task_id):
    tasks = Task.query.get(task_id)
    if tasks == None:
        return jsonify({'error':f'Task id: {task_id} not found'}),404
    db.session.delete(tasks)
    db.session.commit()
    return jsonify({"success":f"Task with id: {task_id} deleted"}), 200
