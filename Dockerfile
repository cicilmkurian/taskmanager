FROM python:3.8
WORKDIR /app
COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY config.py run.py ./
COPY app app
COPY tests tests


EXPOSE 5000
CMD ["python","run.py"]

