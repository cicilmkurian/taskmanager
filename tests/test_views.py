import unittest
from app.models import Task
from config import db, app


class TestView(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        app.config['TESTING'] = True  # Corrected key name
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        with app.app_context():
            db.create_all()

    @classmethod
    def tearDownClass(cls):  # Changed to tearDownClass
        with app.app_context():
            db.drop_all()

    def test_index_route(self):
        with app.test_client() as client:
            response = client.get('/')
            response.status_code != 200

    def test_get(self):
        with app.test_client() as client:
            response = client.get('/tasks')
            response.status_code == 200

    def test_create(self):
        with app.test_client() as client:
            data = {
                "title": " cicil",
                "description": "Sample ",
                "status": "todo",
                "priority": "medium"
            }
            response = client.post('/tasks', json=data)
            response.status_code == 201
            response.data == data

    def test_delete(self):
        with app.app_context():
            with app.test_client() as client:
                task = Task(
                    title=" cicil",
                    description="Sample ",
                    status="todo",
                    priority="medium"
                )
                db.session.add(task)
                db.session.commit()

                response = client.delete(f'/tasks/{task.id}')
                response.status_code == 200
                response.data == {"success": f"Task with id: {task.id} deleted"}

    def test_delete_nonexistent_task(self):
        with app.test_client() as client:
            response = client.delete('/tasks/999')
            response.status_code == 404
            response.json == None


if __name__ == '__main__':
    unittest.main()
