import unittest
from app.models import Task
from config import db, app


class TestView(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        app.config['TESTING'] = True  # Corrected key name
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        with app.app_context():
            db.create_all()

    @classmethod
    def tearDownClass(cls):  # Changed to tearDownClass
        with app.app_context():
            db.drop_all()

    def test_to_dict(self):
        task = Task(id=1, title='Sample Task', description='This is a sample task description', status='todo',
                    priority='medium')

        task_dict = task.to_dict()
        expected_dict = {
            'id': 1,
            'title': 'Sample Task',
            'description': 'This is a sample task description',
            'status': 'todo',
            'priority': 'medium'
        }
        self.assertDictEqual(task_dict, expected_dict)


if __name__ == '__main__':
    unittest.main()
