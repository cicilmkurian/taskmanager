from config import app, db
from app.views import index, get, create, delete

# app entry point
if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.add_url_rule('/', 'index', index, methods=['GET'])
    app.add_url_rule('/tasks', 'get_task', get, methods=['GET'])
    app.add_url_rule('/tasks', 'create_tasks', create, methods=['POST'])
    app.add_url_rule('/tasks/<int:task_id>', 'delete_tasks', delete, methods=['DELETE'])
    app.run()
